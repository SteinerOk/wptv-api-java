wptv-api-java
==========

A Java wrapper around the [Wildpark IPTV API v2][1] using [Retrofit 2][2].

## Usage

Add the following dependency to your Gradle project:

```groovy
compile 'net.wildpark.iptv:wptv-api-java:0.11.0'
```

Or for Maven:

```xml
<dependency>
  <groupId>net.wildpark.iptv</groupId>
  <artifactId>wptv-api-java</artifactId>
  <version>0.11.0</version>
</dependency>
```

## License

    Copyright © 2016 Ihor Kushnirenko

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

 [1]: http://mw.wildpark.net:8080/
 [2]: http://square.github.io/retrofit/
 [3]: https://github.com/ReactiveX/RxJava/