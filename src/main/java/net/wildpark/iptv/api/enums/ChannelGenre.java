/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.enums;

public enum ChannelGenre {
    FAVORITES("0"),
    ALL("1"),
    CHILDREN("8"),
    MOVIE("6"),
    SCIENCE_AND_KNOWLEDGE("9"),
    ENTERTAINMENT("7"),
    SPORTS("4"),
    MUSIC("5"),
    HOBBY("10"),
    NEWS("3"),
    HD_CHANNELS("11"),
    CLOSED("99");

    private final String id;

    ChannelGenre(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}
