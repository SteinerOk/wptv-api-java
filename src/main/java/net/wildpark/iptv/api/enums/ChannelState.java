package net.wildpark.iptv.api.enums;

import org.jetbrains.annotations.NotNull;

public enum ChannelState {
    AVAILABLE(1),
    UNAVAILABLE(0),
    UNALLOWED(-1),
    UNKNOWN(-100);

    private final int id;

    ChannelState(int id) {
        this.id = id;
    }

    @NotNull
    public static ChannelState fromId(final int id) {
        for (@NotNull ChannelState channelState : values()) {
            if (channelState.getId() == id) {
                return channelState;
            }
        }
        return UNKNOWN;
    }

    public int getId() {
        return id;
    }

}
