/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;

public class AuthProfile {

    @Json(name = "ratio")
    private String ratio;
    @Json(name = "volume")
    private String volume;
    @Json(name = "skin")
    private String skin;
    @Json(name = "channel_id")
    private String channelId;
    @Json(name = "genre_id")
    private String genreId;
    @Json(name = "list_pos")
    private String listPos;
    @Json(name = "plmode")
    private String plMode;
    @Json(name = "language")
    private String language;
    @Json(name = "resolution")
    private String resolution;
    @Json(name = "normalize")
    private String normalize;
    @Json(name = "gresolution")
    private String gResolution;
    @Json(name = "clearance")
    private String clearance;
    @Json(name = "volumeontop")
    private String volumeOnTop;
    @Json(name = "confirmation")
    private String confirmation;
    @Json(name = "safe_power_on")
    private String safePowerOn;
    @Json(name = "magnify")
    private String magnify;
    @Json(name = "skip_unavail")
    private String skipUnAvailable;

    public String getRatio() {
        return ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getGenreId() {
        return genreId;
    }

    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

    public String getListPos() {
        return listPos;
    }

    public void setListPos(String listPos) {
        this.listPos = listPos;
    }

    public String getPlMode() {
        return plMode;
    }

    public void setPlMode(String plMode) {
        this.plMode = plMode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getNormalize() {
        return normalize;
    }

    public void setNormalize(String normalize) {
        this.normalize = normalize;
    }

    public String getGResolution() {
        return gResolution;
    }

    public void setGResolution(String gResolution) {
        this.gResolution = gResolution;
    }

    public String getClearance() {
        return clearance;
    }

    public void setClearance(String clearance) {
        this.clearance = clearance;
    }

    public String getVolumeOnTop() {
        return volumeOnTop;
    }

    public void setVolumeOnTop(String volumeOnTop) {
        this.volumeOnTop = volumeOnTop;
    }

    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    public String getSafePowerOn() {
        return safePowerOn;
    }

    public void setSafePowerOn(String safePowerOn) {
        this.safePowerOn = safePowerOn;
    }

    public String getMagnify() {
        return magnify;
    }

    public void setMagnify(String magnify) {
        this.magnify = magnify;
    }

    public String getSkipUnAvailable() {
        return skipUnAvailable;
    }

    public void setSkipUnAvailable(String skipUnAvailable) {
        this.skipUnAvailable = skipUnAvailable;
    }

}