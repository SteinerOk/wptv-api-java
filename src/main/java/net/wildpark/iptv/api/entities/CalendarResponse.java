/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.List;

public class CalendarResponse {

    @Json(name = "items")
    private List<CalendarDay> days = new ArrayList<>();
    @Json(name = "cell_today")
    private int cellToday;
    @Json(name = "cell_selected")
    private int cellSelected;
    @Json(name = "name_month")
    private String nameMonth;
    @Json(name = "channel_id")
    private String channelId;

    public List<CalendarDay> getDays() {
        return days;
    }

    public void setDays(List<CalendarDay> days) {
        this.days = days;
    }

    public int getCellToday() {
        return cellToday;
    }

    public void setCellToday(int cellToday) {
        this.cellToday = cellToday;
    }

    public int getCellSelected() {
        return cellSelected;
    }

    public void setCellSelected(int cellSelected) {
        this.cellSelected = cellSelected;
    }

    public String getNameMonth() {
        return nameMonth;
    }

    public void setNameMonth(String nameMonth) {
        this.nameMonth = nameMonth;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

}