/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;

public class AuthResponse {

    @Json(name = "isActive")
    private boolean active;
    @Json(name = "isAuthenticated")
    private boolean authenticated;
    @Json(name = "profile")
    private AuthProfile profile;
    @Json(name = "update")
    private String update;
    @Json(name = "upd_version")
    private int updVersion;
    @Json(name = "ErrorMsg")
    private String errorMsg;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public AuthProfile getProfile() {
        return profile;
    }

    public void setProfile(AuthProfile profile) {
        this.profile = profile;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    public int getUpdVersion() {
        return updVersion;
    }

    public void setUpdVersion(int updVersion) {
        this.updVersion = updVersion;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}