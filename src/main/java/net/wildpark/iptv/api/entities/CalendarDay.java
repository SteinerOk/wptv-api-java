/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;

public class CalendarDay {

    @Json(name = "day")
    private int day;
    @Json(name = "current_month")
    private boolean currentMonth;
    @Json(name = "holiday")
    private boolean holiday;
    @Json(name = "daytime")
    private long tsStartDay;
    @Json(name = "startUT")
    private String tsStart;
    @Json(name = "endUT")
    private String tsEnd;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public boolean isCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(boolean currentMonth) {
        this.currentMonth = currentMonth;
    }

    public boolean isHoliday() {
        return holiday;
    }

    public void setHoliday(boolean holiday) {
        this.holiday = holiday;
    }

    public long getTsStartDay() {
        return tsStartDay;
    }

    public void setTsStartDay(long tsStartDay) {
        this.tsStartDay = tsStartDay;
    }

    public String getTsStart() {
        return tsStart;
    }

    public void setTsStart(String tsStart) {
        this.tsStart = tsStart;
    }

    public String getTsEnd() {
        return tsEnd;
    }

    public void setTsEnd(String tsEnd) {
        this.tsEnd = tsEnd;
    }

}