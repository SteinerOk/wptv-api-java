/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;

public class RtspParamsResponse {

    @Json(name = "success")
    private boolean success;
    @Json(name = "time_play_now")
    private String timePlayNow;
    @Json(name = "time_play_end")
    private String timePlayEnd;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getTimePlayNow() {
        return timePlayNow;
    }

    public void setTimePlayNow(String timePlayNow) {
        this.timePlayNow = timePlayNow;
    }

    public String getTimePlayEnd() {
        return timePlayEnd;
    }

    public void setTimePlayEnd(String timePlayEnd) {
        this.timePlayEnd = timePlayEnd;
    }

}