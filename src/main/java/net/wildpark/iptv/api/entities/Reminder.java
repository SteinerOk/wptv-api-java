/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;
import net.wildpark.iptv.api.qualifiers.CleanTitle;
import net.wildpark.iptv.api.qualifiers.Timestamp;

public class Reminder {

    @Json(name = "channel_id")
    private String channelId;
    @Json(name = "logo")
    private String channelLogo;
    @CleanTitle
    @Json(name = "program_name")
    private String programTitle;
    @Timestamp
    @Json(name = "timestamp")
    private long tsStart;
    @Timestamp
    @Json(name = "timestamp_end")
    private long tsEnd;
    @Json(name = "genre_id")
    private int genreId;
    @Json(name = "listpos")
    private int listPos;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelLogo() {
        return channelLogo;
    }

    public void setChannelLogo(String channelLogo) {
        this.channelLogo = channelLogo;
    }

    public String getProgramTitle() {
        return programTitle;
    }

    public void setProgramTitle(String programTitle) {
        this.programTitle = programTitle;
    }

    public long getTsStart() {
        return tsStart;
    }

    public void setTsStart(long tsStart) {
        this.tsStart = tsStart;
    }

    public long getTsEnd() {
        return tsEnd;
    }

    public void setTsEnd(long tsEnd) {
        this.tsEnd = tsEnd;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getListPos() {
        return listPos;
    }

    public void setListPos(int listPos) {
        this.listPos = listPos;
    }

}
