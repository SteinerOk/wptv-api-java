/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.List;

public class MessagesResponse {

    @Json(name = "success")
    private boolean success;
    @Json(name = "items")
    private List<MessageShort> shortMessages = new ArrayList<>();

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<MessageShort> getShortMessages() {
        return shortMessages;
    }

    public void setShortMessages(List<MessageShort> shortMessages) {
        this.shortMessages = shortMessages;
    }

}