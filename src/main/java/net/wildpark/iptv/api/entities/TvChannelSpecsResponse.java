/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;

import java.util.ArrayList;
import java.util.List;

public class TvChannelSpecsResponse {

    @Json(name = "success")
    private boolean success;
    @Json(name = "ip")
    private String ip;
    @Json(name = "volume")
    private String volume;
    @Json(name = "def_audio_pid")
    private String defAudioPid;
    @Json(name = "passworded")
    private boolean passworded;
    @Json(name = "programs")
    private List<OsdProgram> programs = new ArrayList<>();
    @Json(name = "ErrorMsg")
    private String errorMsg;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getDefAudioPid() {
        return defAudioPid;
    }

    public void setDefAudioPid(String defAudioPid) {
        this.defAudioPid = defAudioPid;
    }

    public boolean isPassworded() {
        return passworded;
    }

    public void setPassworded(boolean passworded) {
        this.passworded = passworded;
    }

    public List<OsdProgram> getPrograms() {
        return programs;
    }

    public void setPrograms(List<OsdProgram> programs) {
        this.programs = programs;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}