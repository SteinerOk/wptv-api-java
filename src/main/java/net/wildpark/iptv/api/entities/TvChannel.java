/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;
import net.wildpark.iptv.api.enums.ChannelState;

public class TvChannel {

    @Json(name = "id")
    private String id;
    @Json(name = "name")
    private String name;
    @Json(name = "logo")
    private String logo;
    @Json(name = "available")
    private ChannelState state;
    @Json(name = "rtsp")
    private boolean rtsp;
    @Json(name = "favorite")
    private boolean favorite;
    @Json(name = "passworded")
    private boolean passworded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public ChannelState getState() {
        return state;
    }

    public void setState(ChannelState state) {
        this.state = state;
    }

    public boolean isRtsp() {
        return rtsp;
    }

    public void setRtsp(boolean rtsp) {
        this.rtsp = rtsp;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isPassworded() {
        return passworded;
    }

    public void setPassworded(boolean passworded) {
        this.passworded = passworded;
    }

}