/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.entities;

import com.squareup.moshi.Json;
import net.wildpark.iptv.api.qualifiers.CleanTitle;
import net.wildpark.iptv.api.qualifiers.Timestamp;

public class EpgProgram {

    @CleanTitle
    @Json(name = "title")
    private String title;
    @Timestamp
    @Json(name = "timestamp")
    private long tsStart;
    @Timestamp
    @Json(name = "timestamp_end")
    private long tsEnd;
    @Json(name = "reminder")
    private boolean reminder;
    @Json(name = "rtsp")
    private boolean rtsp;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTsStart() {
        return tsStart;
    }

    public void setTsStart(long tsStart) {
        this.tsStart = tsStart;
    }

    public long getTsEnd() {
        return tsEnd;
    }

    public void setTsEnd(long tsEnd) {
        this.tsEnd = tsEnd;
    }

    public boolean isReminder() {
        return reminder;
    }

    public void setReminder(boolean reminder) {
        this.reminder = reminder;
    }

    public boolean isRtsp() {
        return rtsp;
    }

    public void setRtsp(boolean rtsp) {
        this.rtsp = rtsp;
    }

}