/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import net.wildpark.iptv.api.services.IpTvService;
import net.wildpark.iptv.api.services.RxIpTvService;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import java.net.Proxy;

public class WildParkTv {
    public static final String WPTV_URL = "http://mw.wildpark.net:8080/wptv/";
    public static final String WPTV_LOGO_URL = WPTV_URL + "images/1080/channels/";

    @Nullable
    private Device device;
    private OkHttpClient okHttpClient;
    @Nullable
    private Retrofit retrofit;
    private HttpLoggingInterceptor.Level logLevel;
    private boolean ioScheduler;

    private WildParkTv(@NotNull Builder builder) {
        device = builder.device;
        okHttpClient = builder.okHttpClient;
        ioScheduler = builder.ioScheduler;
    }

    @NotNull
    public static Builder newBuilder() {
        return new Builder();
    }

    @NotNull
    public static Builder newBuilder(@NotNull WildParkTv copy) {
        @NotNull Builder builder = new Builder();
        builder.device = copy.device;
        builder.okHttpClient = copy.okHttpClient;
        builder.ioScheduler = copy.ioScheduler;
        return builder;
    }

    @Nullable
    public Device getDevice() {
        return device;
    }

    @NotNull
    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public void updateDevice(@NotNull Device newDevice) {
        OkHttpClient.Builder builder = okHttpClient.newBuilder();
        if (device != null && device.getInterceptor() != null) {
            builder.interceptors().remove(device.getInterceptor());
        }

        device = newDevice;
        builder.addInterceptor(device.getInterceptor());
        builder.cookieJar(Helper.getCookieManager());
        okHttpClient = builder.build();
        retrofit = null;
    }

    public void changeLogLevel(@NotNull HttpLoggingInterceptor.Level newLevel) {
        if (logLevel == newLevel) return;

        logLevel = newLevel;
        for (Interceptor interceptor : okHttpClient.interceptors()) {
            if (interceptor instanceof HttpLoggingInterceptor) {
                ((HttpLoggingInterceptor) interceptor).setLevel(logLevel);
            }
        }
        retrofit = null;
    }

    public void setProxy(@NotNull Proxy newProxy) {
        okHttpClient = okHttpClient.newBuilder()
                .proxy(newProxy)
                .build();
        retrofit = null;
    }

    @NotNull
    protected Retrofit getRetrofit() {
        if (retrofit == null) {
            @NotNull Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(WPTV_URL)
                    .addConverterFactory(MoshiConverterFactory.create(Helper.getMoshi()))
                    .client(okHttpClient);
            if (ioScheduler) {
                builder.addCallAdapterFactory(RxJava2CallAdapterFactory
                        .createWithScheduler(io.reactivex.schedulers.Schedulers.io()));
            } else {
                builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());
            }
            retrofit = builder.build();
        }
        return retrofit;
    }

    public IpTvService ipTvService() {
        return getRetrofit().create(IpTvService.class);
    }

    public RxIpTvService rxIpTvService() {
        return getRetrofit().create(RxIpTvService.class);
    }

    public static final class Builder {
        private Device device;
        private OkHttpClient okHttpClient;
        private HttpLoggingInterceptor.Level logLevel;
        private String proxyHostname;
        private int proxyPort;
        private boolean ioScheduler;

        private Builder() {
        }

        @NotNull
        public Builder device(Device device) {
            this.device = device;
            return this;
        }

        @NotNull
        public Builder okHttpClient(OkHttpClient okHttpClient) {
            this.okHttpClient = okHttpClient;
            return this;
        }

        @NotNull
        public Builder logLevel(HttpLoggingInterceptor.Level level) {
            this.logLevel = level;
            return this;
        }

        @NotNull
        public Builder proxy(String hostname, int port) {
            this.proxyHostname = hostname;
            this.proxyPort = port;
            return this;
        }

        @NotNull
        public Builder ioScheduler(boolean useIoScheduler) {
            this.ioScheduler = useIoScheduler;
            return this;
        }

        @NotNull
        public WildParkTv build() {
            @NotNull OkHttpClient.Builder builder = okHttpClient == null
                    ? Helper.getOkHttpClientBuilder() : okHttpClient.newBuilder();
            builder.addInterceptor(Helper.getLogger(logLevel == null
                    ? HttpLoggingInterceptor.Level.NONE : logLevel));
            if (proxyHostname != null && proxyPort > 0) {
                builder.proxy(Helper.getHttpProxy(proxyHostname, proxyPort));
            }
            if (device != null && device.getInterceptor() != null) {
                builder.addInterceptor(device.getInterceptor());
            }
            okHttpClient = builder.build();
            return new WildParkTv(this);
        }
    }
}
