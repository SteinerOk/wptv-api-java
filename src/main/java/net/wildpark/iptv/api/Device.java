/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public class Device {
    @NotNull
    private Type type;
    @NotNull
    private String mac;
    @NotNull
    private String sn;
    @Nullable
    private Interceptor interceptor;

    public Device(@Nullable Type type, @Nullable String mac, @Nullable String sn) {
        if (type != null) {
            this.type = type;
        } else {
            this.type = Type.UNKNOWN;
        }
        if (mac == null || mac.isEmpty()) {
            throw new IllegalArgumentException("Device MAC required");
        }
        if (sn == null || sn.isEmpty()) {
            throw new IllegalArgumentException("Device SN required");
        }
        this.mac = mac;
        this.sn = sn;
        switch (this.type) {
            case MAG_250:
            case MAG_254:
                interceptor = new Mag250HeadersInterceptor();
                break;
            default:
                break;
        }
    }

    @NotNull
    public Type getType() {
        return type;
    }

    @NotNull
    public String getMac() {
        return mac;
    }

    @NotNull
    public String getSn() {
        return sn;
    }

    @Nullable
    public Interceptor getInterceptor() {
        return interceptor;
    }

    public enum Type {
        MAG_250(1, "mag_250", "MAG 250"),
        MAG_254(2, "mag_254", "MAG 254"),
        ALBIS(3, "albis", "ALBIS"),
        UNKNOWN(0, "unknown", "Не известно");

        private int id;
        private String codeName;
        private String label;

        Type(int id, String codeName, String label) {
            this.id = id;
            this.codeName = codeName;
            this.label = label;
        }

        @NotNull
        public static Type fromId(final int id) {
            for (@NotNull Type deviceType : values()) {
                if (deviceType.getId() == id) {
                    return deviceType;
                }
            }
            return UNKNOWN;
        }

        @NotNull
        public static Type fromCodeName(@Nullable final String codeName) {
            if (codeName != null) {
                for (@NotNull Type deviceType : values()) {
                    if (codeName.equals(deviceType.getCodeName())) {
                        return deviceType;
                    }
                }
            }
            return UNKNOWN;
        }

        public int getId() {
            return id;
        }

        public String getCodeName() {
            return codeName;
        }

        public String getLabel() {
            return label;
        }

    }

    public static class Mag250HeadersInterceptor implements Interceptor {
        private static final String HEADER_USER_AGENT = "User-Agent";
        private static final String MAG250_USER_AGENT = "Mozilla/5.0 (QtEmbedded; U; Linux; C) AppleWebKit/533.3 (KHTML, like Gecko) MAG200 stbapp ver: 4 rev: 724 Mobile Safari/533.3";

        @Override
        public Response intercept(@NotNull Chain chain) throws IOException {
            Request request = chain.request().newBuilder()
                    .removeHeader(HEADER_USER_AGENT)
                    .addHeader(HEADER_USER_AGENT, MAG250_USER_AGENT)
                    .build();
            return chain.proceed(request);
        }
    }
}
