package net.wildpark.iptv.api.adapters;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;
import net.wildpark.iptv.api.qualifiers.CleanDesc;
import net.wildpark.iptv.api.qualifiers.CleanTitle;
import net.wildpark.iptv.api.qualifiers.Timestamp;

public class QualifiersAdapter {

    @FromJson
    @CleanTitle
    public String titleFromJson(String dirtyTitle) {
        if (dirtyTitle == null || dirtyTitle.length() < 1) {
            return "N/A";
        } else if ((dirtyTitle.endsWith(".") && !dirtyTitle.endsWith("с.") && !dirtyTitle.endsWith("ч.")
                && !dirtyTitle.endsWith("эп.")) || dirtyTitle.endsWith(". *")) {
            return dirtyTitle.substring(0, dirtyTitle.lastIndexOf(".")).trim();
        } else if (dirtyTitle.endsWith(" *")) {
            return dirtyTitle.substring(0, dirtyTitle.lastIndexOf(" ")).trim();
        }
        return dirtyTitle.trim();
    }

    @ToJson
    public String titleToJson(@CleanTitle String cleanTitle) {
        return cleanTitle;
    }

    @FromJson
    @CleanDesc
    public String descFromJson(String dirtyDesc) {
        if (dirtyDesc != null && dirtyDesc.length() > 0) {
            return dirtyDesc.replaceAll("<br>|<br/>|<br />", " ").replaceAll("\\<.*?>", "").trim();
        }
        return "";
    }

    @ToJson
    public String descToJson(@CleanDesc String cleanDesc) {
        return cleanDesc;
    }

    @FromJson
    @Timestamp
    public long timestampFromJson(String tsStr) {
        try {
            return Long.parseLong(tsStr.trim());
        } catch (NumberFormatException e) {
            return -1L;
        }
    }

    @ToJson
    public long timestampToJson(@Timestamp long timestamp) {
        return timestamp;
    }

}
