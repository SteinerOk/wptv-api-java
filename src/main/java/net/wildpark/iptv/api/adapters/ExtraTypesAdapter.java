package net.wildpark.iptv.api.adapters;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.ToJson;
import net.wildpark.iptv.api.enums.ChannelState;

import java.io.IOException;

public class ExtraTypesAdapter {

    @FromJson
    public int channelStateIdFromJson(JsonReader reader) throws IOException {
        if (reader.peek() == JsonReader.Token.NUMBER) {
            return reader.nextInt();
        } else if (reader.peek() == JsonReader.Token.BOOLEAN) {
            return reader.nextBoolean() ? 1 : -1;
        } else {
            reader.skipValue();
            return 0;
        }
    }

    @FromJson
    public ChannelState channelStateFromJson(int stateId) {
        return ChannelState.fromId(stateId);
    }

    @ToJson
    public int channelStateToJson(ChannelState state) {
        return state.getId();
    }

}
