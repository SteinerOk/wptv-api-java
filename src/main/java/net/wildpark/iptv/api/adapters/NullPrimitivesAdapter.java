package net.wildpark.iptv.api.adapters;

import com.squareup.moshi.FromJson;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class NullPrimitivesAdapter {

    @FromJson
    public boolean booleanFromJson(@Nullable Boolean value) {
        if (value == null) {
            return false;
        }
        return value;
    }

    @FromJson
    public int intFromJson(@Nullable Integer value) {
        if (value == null) {
            return 0;
        }
        return value;
    }

    @FromJson
    public double doubleFromJson(@Nullable Double value) {
        if (value == null) {
            return -1D;
        }
        return value;
    }

    @FromJson
    public long longFromJson(@Nullable Long value) {
        if (value == null) {
            return -1L;
        }
        return value;
    }

    @Retention(RetentionPolicy.RUNTIME)
    @interface Nullable {
    }

}
