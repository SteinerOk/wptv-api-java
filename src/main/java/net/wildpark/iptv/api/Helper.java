/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api;

import com.squareup.moshi.JsonWriter;
import com.squareup.moshi.Moshi;
import net.wildpark.iptv.api.adapters.ExtraTypesAdapter;
import net.wildpark.iptv.api.adapters.NullPrimitivesAdapter;
import net.wildpark.iptv.api.adapters.QualifiersAdapter;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.concurrent.TimeUnit;

public class Helper {
    public static final int CONNECT_TIMEOUT_SEC = 10;
    public static final int READ_TIMEOUT_SEC = 15;
    public static final int WRITE_TIMEOUT_SEC = 30;

    private Helper() {
    }

    @NotNull
    public static OkHttpClient.Builder getOkHttpClientBuilder() {
        return new OkHttpClient.Builder()
                .connectTimeout(Helper.CONNECT_TIMEOUT_SEC, TimeUnit.SECONDS)
                .writeTimeout(Helper.WRITE_TIMEOUT_SEC, TimeUnit.SECONDS)
                .readTimeout(Helper.READ_TIMEOUT_SEC, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .cookieJar(Helper.getCookieManager())
                .addInterceptor(new Helper.WpTvHeadersInterceptor());
    }

    @NotNull
    public static JavaNetCookieJar getCookieManager() {
        @NotNull CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        return new JavaNetCookieJar(cookieManager);
    }

    @NotNull
    public static Interceptor getLogger(@NotNull HttpLoggingInterceptor.Level level) {
        @NotNull HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(level);
        return loggingInterceptor;
    }

    @NotNull
    public static Proxy getHttpProxy(@NotNull String hostname, int port) {
        @NotNull InetSocketAddress address = new InetSocketAddress(hostname, port);
        return new Proxy(Proxy.Type.HTTP, address);
    }

    @NotNull
    public static Moshi getMoshi() {
        return new Moshi.Builder()
                .add(new QualifiersAdapter())
                .add(new ExtraTypesAdapter())
                .add(new NullPrimitivesAdapter())
                .build();
    }

    @NotNull
    public static String cryptPinCode(@NotNull String sn, @NotNull String pinCode) {
        if (sn.length() < 5 || pinCode.length() != 4) {
            throw new IllegalArgumentException("Incorrect serial number or pin code!");
        }

        @NotNull String key = sn.substring(sn.length() - 4, sn.length());
        @NotNull StringBuilder builder = new StringBuilder();
        for (int i = 0; i < pinCode.length(); i++) {
            builder.append((char) (Character.codePointAt(pinCode, i) ^ Integer.parseInt(String.valueOf(key.charAt(i)))));
        }
        return builder.toString();
    }

    @NotNull
    public static String prettyJsonFromObject(Object obj) throws IOException {
        final Buffer buffer = new Buffer();

        final JsonWriter jsonWriter = JsonWriter.of(buffer);
        jsonWriter.setSerializeNulls(true);
        jsonWriter.setIndent("   ");

        final Moshi moshi = getMoshi();
        final Class type = obj.getClass();
        moshi.adapter(type).toJson(jsonWriter, obj);
        return buffer.readUtf8();
    }

    public static class WpTvHeadersInterceptor implements Interceptor {

        @Override
        public Response intercept(@NotNull Chain chain) throws IOException {
            Request request = chain.request().newBuilder()
                    .removeHeader("Accept")
                    .addHeader("Accept", "application/json, text/javascript, */*; q=0.01")
                    .removeHeader("Connection")
                    .addHeader("Connection", "Keep-Alive")
                    .addHeader("Accept-Encoding", "gzip, deflate, sdch")
                    .addHeader("Accept-Language", "ru,en-US;q=0.8,en;q=0.6,uk;q=0.4")
                    .addHeader("X-Requested-With", "XMLHttpRequest")
                    .addHeader("Referer", "http://mw.wildpark.net:8080/")
                    .build();
            return chain.proceed(request);
        }
    }
}
