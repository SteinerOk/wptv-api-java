/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.services;

import net.wildpark.iptv.api.entities.*;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IpTvService {

    @GET("ws/auth_stb.php")
    Call<AuthResponse> authenticate(
            @Query("mac") String mac,
            @Query("sn") String sn
    );

    @GET("ws/keep-alive.php")
    Call<KeepAliveResponse> keepAlive();

    @GET("ws/get_IptvMenu.php")
    Call<TvChannelGenresResponse> getChannelGenres();

    @GET("ws/get_channels.php?genreId=1 ")
    Call<TvChannelsResponse> getAllChannels();

    @GET("ws/get_channels.php")
    Call<TvChannelsResponse> getChannels(
            @Query("genreId") String channelGenreId
    );

    @GET("ws/get_channels.php")
    Call<TvChannelsResponse> getChannelsByPage(
            @Query("genreId") String channelGenreId,
            @Query("perPage") int perPage,
            @Query("page") int page
    );

    @GET("ws/get_epg.php")
    Call<EpgResponse> getEpgByPage(
            @Query("channelId") String channelId,
            @Query("perPage") int perPage,
            @Query("page") int page
    );

    /*
    * tsStart - Unix Timestamp in seconds
    * tsEnd - Unix Timestamp in seconds
    */
    @GET("ws/get_epg.php")
    Call<EpgResponse> getEpgByTime(
            @Query("channelId") String channelId,
            @Query("startUT") long tsStart,
            @Query("endUT") long tsEnd
    );

    /*
    * tsStart - Unix Timestamp in seconds
    * tsEnd - Unix Timestamp in seconds
    */
    @GET("ws/get_epg.php?withnow=1")
    Call<EpgResponse> getEpgByTimeWithNow(
            @Query("channelId") String channelId,
            @Query("startUT") long tsStart,
            @Query("endUT") long tsEnd
    );

    @GET("ws/get_channel_ip.php")
    Call<TvChannelSpecsResponse> getChannelSpecs(
            @Query("id") String channelId
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/get_descr.php")
    Call<ProgramDescResponse> getProgramDesc(
            @Query("channel_id") String channelId,
            @Query("ts") long timestamp
    );

    /*
    * words - ключевые слова для поиска через запятую, например: words:новости,неделя
    */
    @GET("ws/epg_search.php")
    Call<SearchEpgResponse> searchEpg(
            @Query("channel_id") String channelId,
            @Query("genre_id") String channelGenreId,
            @Query("words[]") String words
    );

    @GET("ws/get_osd.php")
    Call<OsdResponse> getOsd(
            @Query("channelId") String channelId
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/get_osd.php")
    Call<OsdResponse> getOsdByTime(
            @Query("channelId") String channelId,
            @Query("timestamp") long timestamp
    );

    @GET("ws/top10_channels.php")
    Call<TopTvChannelsResponse> getTopChannels(
            @Query("genre_id") String channelGenreId
    );

    @GET("ws/get_messages.php")
    Call<MessagesResponse> getMessages();

    @GET("ws/get_messages.php")
    Call<MessageResponse> getMessage(
            @Query("message_id") String messageId
    );

    @GET("ws/favorites.php")
    Call<RequestResponse> addOrDeleteFavorite(
            @Query("iptvId") String channelId
    );

    /*
    * sortedList - id каналов через запятую, например: sortfav:76,58,125,59,106,8,55,12
    */
    @GET("ws/favorites_sort.php")
    Call<RequestResponse> sort(
            @Query("genre_id") String channelGenreId,
            @Query("sortfav") String sortedList
    );

    @GET("ws/get_reminder.php")
    Call<RemindersResponse> getAllReminders();

    @GET("ws/get_reminder.php")
    Call<RemindersResponse> getGenreReminders(
            @Query("genre_id") String channelGenreId
    );

    /*
    * id канала и Unix Timestamp через разделитель, например: data[]:58|1468800300
    */
    @GET("ws/reminder.php")
    Call<RequestResponse> addOrDeleteReminder(
            @Query("data[]") String channelPipeTimestamp
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/calendar.php")
    Call<CalendarResponse> getCalendar(
            @Query("channel_id") String channelId,
            @Query("unixtime") long timestamp
    );

    @GET("ws/bookmarks.php?getlist=true")
    Call<TimeShiftMarksResponse> getTimeShiftMarks();

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/bookmarks.php")
    Call<RequestResponse> addTimeShiftMark(
            @Query("channel_id") String channelId,
            @Query("ts") long timestamp
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/ts_check.php")
    Call<RtspLinkResponse> getRtspLink(
            @Query("channel_id") String channelId,
            @Query("ts") long timestamp
    );

    @GET("ws/get_rtsp_params.php")
    Call<RtspParamsResponse> getRtspParams();

    @GET("ws/password.php")
    Call<RequestResponse> checkPinCode(
            @Query("pass") String encryptedPinCode
    );

    @GET("ws/password.php")
    Call<RequestResponse> changePinCode(
            @Query("newpass") String encryptedNewPinCode
    );

    @GET("ws/password.php")
    Call<RequestResponse> lockOrUnLockChannel(
            @Query("channel_id") String channelId
    );

    @GET("ws/get_images.php")
    Call<ImagesResponse> getImages();

}
