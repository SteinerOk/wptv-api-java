/*
 * Copyright © 2016 Ihor Kushnirenko
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.wildpark.iptv.api.services;

import io.reactivex.Completable;
import io.reactivex.Single;
import net.wildpark.iptv.api.entities.*;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RxIpTvService {

    @GET("ws/auth_stb.php")
    Single<AuthResponse> authenticate(
            @Query("mac") String mac,
            @Query("sn") String sn
    );

    @GET("ws/keep-alive.php")
    Completable keepAlive();

    @GET("ws/get_IptvMenu.php")
    Single<TvChannelGenresResponse> getChannelGenres();

    @GET("ws/get_channels.php?genreId=1 ")
    Single<TvChannelsResponse> getAllChannels();

    @GET("ws/get_channels.php")
    Single<TvChannelsResponse> getChannels(
            @Query("genreId") String channelGenreId
    );

    @GET("ws/get_channels.php")
    Single<TvChannelsResponse> getChannelsByPage(
            @Query("genreId") String channelGenreId,
            @Query("perPage") int perPage,
            @Query("page") int page
    );

    @GET("ws/get_epg.php")
    Single<EpgResponse> getEpgByPage(
            @Query("channelId") String channelId,
            @Query("perPage") int perPage,
            @Query("page") int page
    );

    /*
    * tsStart - Unix Timestamp in seconds
    * tsEnd - Unix Timestamp in seconds
    */
    @GET("ws/get_epg.php")
    Single<EpgResponse> getEpgByTime(
            @Query("channelId") String channelId,
            @Query("startUT") long tsStart,
            @Query("endUT") long tsEnd
    );

    /*
    * tsStart - Unix Timestamp in seconds
    * tsEnd - Unix Timestamp in seconds
    */
    @GET("ws/get_epg.php?withnow=1")
    Single<EpgResponse> getEpgByTimeWithNow(
            @Query("channelId") String channelId,
            @Query("startUT") long tsStart,
            @Query("endUT") long tsEnd
    );

    @GET("ws/get_channel_ip.php")
    Single<TvChannelSpecsResponse> getChannelSpecs(
            @Query("id") String channelId
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/get_descr.php")
    Single<ProgramDescResponse> getProgramDesc(
            @Query("channel_id") String channelId,
            @Query("ts") long timestamp
    );

    /*
    * words - ключевые слова для поиска через запятую, например: words:новости,неделя
    */
    @GET("ws/epg_search.php")
    Single<SearchEpgResponse> searchEpg(
            @Query("channel_id") String channelId,
            @Query("genre_id") String channelGenreId,
            @Query("words[]") String words
    );

    @GET("ws/get_osd.php")
    Single<OsdResponse> getOsd(
            @Query("channelId") String channelId
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/get_osd.php")
    Single<OsdResponse> getOsdByTime(
            @Query("channelId") String channelId,
            @Query("timestamp") long timestamp
    );

    @GET("ws/top10_channels.php")
    Single<TopTvChannelsResponse> getTopChannels(
            @Query("genre_id") String channelGenreId
    );

    @GET("ws/get_messages.php")
    Single<MessagesResponse> getMessages();

    @GET("ws/get_messages.php")
    Single<MessageResponse> getMessage(
            @Query("message_id") String messageId
    );

    @GET("ws/favorites.php")
    Single<RequestResponse> addOrDeleteFavorite(
            @Query("iptvId") String channelId
    );

    /*
    * sortedList - id каналов через запятую, например: sortfav:76,58,125,59,106,8,55,12
    */
    @GET("ws/favorites_sort.php")
    Single<RequestResponse> sort(
            @Query("genre_id") String channelGenreId,
            @Query("sortfav") String sortedList
    );

    @GET("ws/get_reminder.php")
    Single<RemindersResponse> getAllReminders();

    @GET("ws/get_reminder.php")
    Single<RemindersResponse> getGenreReminders(
            @Query("genre_id") String channelGenreId
    );

    /*
    * id канала и Unix Timestamp через разделитель, например: data[]:58|1468800300
    */
    @GET("ws/reminder.php")
    Single<RequestResponse> addOrDeleteReminder(
            @Query("data[]") String channelPipeTimestamp
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/calendar.php")
    Single<CalendarResponse> getCalendar(
            @Query("channel_id") String channelId,
            @Query("unixtime") long timestamp
    );

    @GET("ws/bookmarks.php?getlist=true")
    Single<TimeShiftMarksResponse> getTimeShiftMarks();

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/bookmarks.php")
    Single<RequestResponse> addTimeShiftMark(
            @Query("channel_id") String channelId,
            @Query("ts") long timestamp
    );

    /*
    * timestamp - Unix Timestamp in seconds
    */
    @GET("ws/ts_check.php")
    Single<RtspLinkResponse> getRtspLink(
            @Query("channel_id") String channelId,
            @Query("ts") long timestamp
    );

    @GET("ws/get_rtsp_params.php")
    Single<RtspParamsResponse> getRtspParams();

    @GET("ws/password.php")
    Single<RequestResponse> checkPinCode(
            @Query("pass") String encryptedPinCode
    );

    @GET("ws/password.php")
    Single<RequestResponse> changePinCode(
            @Query("newpass") String encryptedNewPinCode
    );

    @GET("ws/password.php")
    Single<RequestResponse> lockOrUnLockChannel(
            @Query("channel_id") String channelId
    );

    @GET("ws/get_images.php")
    Single<ImagesResponse> getImages();

}
