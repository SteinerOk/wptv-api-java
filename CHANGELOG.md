Change Log
==========


## 0.12.0

_2016-11-13_

* Change the names of entities to resolve conflicts with Android TV Input Framework

## 0.11.0

_2016-11-12_

* Replace RxJava1 to RxJava2 ([What's different in RxJava 2.0](https://github.com/ReactiveX/RxJava/wiki/What's-different-in-2.0)).
* Change  entity name "Category" for resolve conflict with enum.

## 0.10.0

_2016-10-27_

* Major changes in entities.
* Fix some inconsistencies between API responses and entities.
* Replace Gson on Moshi.
* Rx IO scheduler is optional now.

## 0.9.1

_2016-08-23_

* Add Genres enumerations.

## 0.9.0

_2016-08-23_

* Initial release supporting Wildpark IPTV API v2.7.1 (settings API is unavailable).
* JavaDoc is currently unavailable.